﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardBehaviorScript : MonoBehaviour {
    public GUISkin scoreLbl;
    public GUISkin timerlbl;
    public float timer = 8f;

    List<GameObject> ListPrefab = new List<GameObject>();
    float x = -16f;
    float y = 6f;

    void Start()
    {
        
        ListPrefab.AddRange(Resources.LoadAll<GameObject>("prefab"));
        ListPrefab.AddRange(Resources.LoadAll<GameObject>("prefab"));
        Debug.Log(ListPrefab.Count);

        for (int i = 0; i < 36; i++)
        {
            int RandomPosition = Random.Range(0, ListPrefab.Count);
            GameObject FrontCardCreator = Instantiate(ListPrefab[RandomPosition], new Vector3(x, y, 0), transform.rotation) as GameObject;
            x += 4f;
            if ((i + 1) % 9 == 0)
            {
                x = -16f;
                y -= 4f;
            }
            ListPrefab.RemoveAt(RandomPosition);
        }
    }

    // Update is called once per frame
    void Update () {
        timer = timer + Time.deltaTime;
    }
    void OnGUI()

    {
        GUI.skin = scoreLbl;
        GUI.Label(new Rect(300, 8, 200, 300), "Score: "+ OnClick.GameScore); 

        GUI.skin = timerlbl;
        GUI.Label(new Rect(400, 8, 200, 300), "Time: "+ Mathf.Round(timer));
    }
}
