﻿using UnityEngine;
using System.Collections;


 
public class OnClick : MonoBehaviour {
    
    public GameObject prefablists = null;
    public int cards = 36;
    float timer = 2f;
    public static bool firstcard = false; // First click
    public static bool secondcard = false; // Second click
    public static string FirstCardTag; //Stores the gameobject tag of the Card clicked first
    public static string SecondCardTag; //Stores the gameobject tag of the Card clicked second
    public static GameObject FirstClick; //Stores the gameobject of the first click
    public static int GameScore = 0; 
    public AudioClip[] audioClip; //Creating an array to store all the audio tracks.

    void Start () {
        //Plays the first sound clip in the array.
        PlaySound(0);
        //Calls the CoRoutine required to flip the cards.
        StartCoroutine(Cardsflip());
        
    }
    void Update()
    {

    }
    void OnMouseDown()
    {
        Debug.Log(firstcard);
        Debug.Log(secondcard);

        if (firstcard == false)
        {
            //When the user clicks the code will make the card visible and store the gametag of the gameobject in a variable.
            //While removing its collider so the same card cannot be clicked again and Playing the second sound clip in the array.
            firstcard = true;
            FirstClick = gameObject;
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            FirstCardTag = gameObject.tag;
            Debug.Log(FirstCardTag);
            Debug.Log(FirstClick);
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            PlaySound(1);
            
        }

        else if (secondcard == false)
        {
            //When the player has clicked again the code will do the same as it did for the first card.
            secondcard = true; 
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            SecondCardTag = gameObject.tag;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            Debug.Log(SecondCardTag);
            PlaySound(2);
            
            if (FirstCardTag == SecondCardTag)
            {
                //This will compare the game tags to see if a match has been made
                secondcard = false;
                firstcard = false;
                FirstClick.GetComponent<BoxCollider2D>().enabled = false;
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
                GameScore += 1;
                PlaySound(4);
                Debug.Log("Match Success");
                Debug.Log(FirstCardTag);
                Debug.Log(SecondCardTag);

                if(GameScore == 2)
                {
                    // once the game finished a game ending sound clip will play.
                    Debug.Log("Game Finished Welldone");
                    PlaySound(6);
                    Application.Quit();
                }
                

            }
            else
            {
                //if the cards dont match they will become invisible again while playing a mismatch sound track.
                Debug.Log("Cards don't match");
                Debug.Log(FirstCardTag);
                Debug.Log(SecondCardTag);
                secondcard = false;
                firstcard = false;
                PlaySound(5);
                FirstClick.GetComponent<BoxCollider2D>().enabled = true;
                gameObject.GetComponent<BoxCollider2D>().enabled = true;
                StartCoroutine(HidingCards());
                
                
            }
        }
    }
    public void PlaySound(int clip)
    {
        //This code will get the audio source and play a clip which was put into the array through unity.
        // 0 = Shuffle  1 = FlipCardSound  2 = FlipCardSound2  3 = FlippingBack  4 = Match  5 = NotMatch
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = audioClip[clip];
        audio.Play();
    }        

    IEnumerator HidingCards()
    {
        //This code is used to turn over the wrongly matched cards 
        yield return new WaitForSeconds(1f);
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        FirstClick.GetComponent<SpriteRenderer>().enabled = false;
        PlaySound(3);
    }
    IEnumerator Cardsflip()
    {
        //When the game starts this code will execute which will make the cards hidden asif they were turned over.
        yield return new WaitForSeconds(4f);
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        
    }
    // Update is called once per frame
    
}
